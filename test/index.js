var rpg = require('../dist/index');
var pokedex = rpg.Constants.Pokedex;

var TrainerPokemon = rpg.Pokemon.pokemonCreate({
    id: 128,
    level: 100,
    color: 'shiny',
    moves: ['toxic', 'flash', 'thunderbolt', 'spark']
});

var OpponentPokemon = rpg.Pokemon.pokemonCreate({
    id: 3,
    level: 75,
    moves: [ 'swords dance', 'dark void' ]
});

var trainerPokemon = rpg.Battle.pokemonCreate(TrainerPokemon.getBattlePokemonReady());

var opponentPokemon = rpg.Battle.pokemonCreate(OpponentPokemon.getBattlePokemonReady());
console.log(trainerPokemon)
console.log(opponentPokemon.Attack(opponentPokemon.battle_moves[0], trainerPokemon).logs)
console.log(opponentPokemon.Attack(opponentPokemon.battle_moves[0], trainerPokemon).logs)
console.log(opponentPokemon.Attack(opponentPokemon.battle_moves[0], trainerPokemon).logs)
console.log(opponentPokemon.Attack(opponentPokemon.battle_moves[0], trainerPokemon).logs)
console.log(opponentPokemon.Attack(opponentPokemon.battle_moves[0], trainerPokemon).logs)
console.log(opponentPokemon.Attack(opponentPokemon.battle_moves[0], trainerPokemon).logs)

// console.log(JSON.stringify(TrainerPokemon));
// console.log("VS");
// console.log(JSON.stringify(OpponentPokemon));

// console.log(`\n`);

// console.log(TrainerPokemon.Attack(TrainerPokemon.battle_moves[0], OpponentPokemon))

// console.log(OpponentPokemon.Attack(OpponentPokemon.battle_moves[0], TrainerPokemon))
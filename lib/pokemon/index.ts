import Pokemon from '../core/model/pokemon';

import { KantoDefaultMoveList } from '../core/constant/default_move_list';

import { getPokemonByName, getPokemonById, IPokedexData } from '../core/constant/pokedex';

import { IPokemonCreateRequest } from '../core/interface/request';

import RandomItems from 'random-items';

import MoveIndex from '../core/constant/moves';

export function pokemonCreate(input: IPokemonCreateRequest): Pokemon {
    let dex_data: IPokedexData = {} as IPokedexData;
    let pokemon: Pokemon = {} as Pokemon;

    if (input.pokemon_id) {
        dex_data = getPokemonById(input.pokemon_id);
    } else if (input.name) {
        dex_data = getPokemonByName(input.name);
    }

    const moves = input.moves || RandomItems(Object.keys(MoveIndex), 4);

    pokemon = new Pokemon({
        name: dex_data.species,
        color: input.color,
        types: dex_data.types,
        level: input.level || 1,
        hp: input.hp || dex_data.baseStats.hp,
        attack: input.attack || dex_data.baseStats.attack,
        defense: input.defense || dex_data.baseStats.defense,
        special_attack: input.special_attack || dex_data.baseStats.special_attack,
        special_defense: input.special_defense || dex_data.baseStats.special_defense,
        speed: input.speed || dex_data.baseStats.speed,
        moves: moves,
        inventory: 1,
    });

    return pokemon;
}

export default { pokemonCreate };

import { IItem } from '../interface/items';

export default class Item implements IItem {
    name;
    healAmount; //how much does it heal
    healStat; //which stat does it heal

    constructor(item: IItem) {
        this.name = item.name;
        this.healAmount = item.healAmount; //how much does it heal
        this.healStat = item.healStat; //which stat does it heal
    }

    heal(target) { //heals HP of a Pokemon. Called by Pokemon.heal method
        var amt = this.healAmount; //how much the item heals
        var stat = this.healStat; //which stat will be raised

        target.raiseStat(stat, amt); //calls pokemon.raiseStat method
    };
}
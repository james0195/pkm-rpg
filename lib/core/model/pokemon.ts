import { IPokemon } from '../interface/pokemon';
import { IBattlePokemonReady } from '../interface/battle';

import { getPokemonByName } from '../constant/pokedex';
import { MoveLoad } from '../methods/moves';

export default class Pokemon implements IPokemon {

    name;
    color;
    level = 1;
    types;

    hp;
    attack;
    defense;
    special_attack;
    special_defense;
    speed;
    moves;

    moves_pp_used;

    inventory = 1;
    is_battle_ready;

    input;

    constructor(pokemon: IPokemon) {

        this.name = pokemon.name;
        this.color = pokemon.color;

        this.types = pokemon.types;

        this.hp = pokemon.hp;
        this.attack = pokemon.attack;
        this.defense = pokemon.defense;
        this.special_attack = pokemon.special_attack;
        this.special_defense = pokemon.special_defense;
        this.speed = pokemon.speed;

        this.moves = pokemon.moves;

        this.input = pokemon;

        this.setLevel(pokemon.level || this.level);
    }

    getPokemon(): IPokemon {
        const input = this.input;

        Object.keys(input).forEach(prop => {
            input[prop] = this[prop];
        });

        return input;
    }

    getBattlePokemonReady(): IBattlePokemonReady {
        const self = this.getPokemon();

        let battleReady = {} as IBattlePokemonReady;

        if(!this.is_battle_ready) {
            battleReady.status_effect = "";

            battleReady.accuracy = 100;
            battleReady.evasion = 100;
            battleReady.max_accuracy = 100;
            battleReady.max_evasion = 100;

            battleReady.max_hp = this.hp;
            battleReady.max_attack = this.attack;
            battleReady.max_defense = this.defense;
            battleReady.max_special_attack = this.special_attack;
            battleReady.max_special_defense = this.special_defense;
            battleReady.max_speed = this.speed;

            battleReady.moves = this.moves;
            battleReady.battle_moves = this.moves.map(m => MoveLoad(m));

            battleReady.moves_pp_used = this.moves.map(m => 0);

            battleReady.is_battle_ready = true;
        } else {
            battleReady = {
                moves_pp_used: this.moves_pp_used
            } as IBattlePokemonReady;
        }

        return { ...self, ...battleReady };
    }

    getStats() {

        const name = this.name;
        const color = this.color;

        const types = this.types;
        const level = this.level;
        const hp = this.hp;
        const attack = this.attack;
        const defense = this.defense;
        const special_attack = this.special_attack;
        const special_defense = this.special_defense;
        const speed = this.speed;

        const moves = this.moves;

        return {
            name,
            color,
            types,
            level,
            hp,
            attack,
            defense,
            special_attack,
            special_defense,
            speed,
            moves
        };
    }

    setLevel(level: number) {
        const pokemon = getPokemonByName(this.name);
        const stats = ['hp', 'attack', 'defense', 'special_attack', 'special_defense', 'speed'];

        this.level = level;

        stats.forEach(stat => {
            this[stat] = Math.round(level * (pokemon.baseStats[stat] * (1 / 5)));
        });
    }

}

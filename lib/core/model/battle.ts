import Move from './moves';
import Numeral from 'numeral';
import Pokemon from "./pokemon";
import RandomOutcome from 'random-outcome';

import { MoveLoad } from '../methods/moves';
import { Stats } from "../constant";
import { IBattlePokemon, IBattlePokemonReady } from "../interface";

export class BattlePokemon extends Pokemon implements IBattlePokemon {
    status_condition;

    accuracy;
    evasion;
    max_accuracy;
    max_evasion;


    max_hp;
    max_attack;
    max_defense;
    max_special_attack;
    max_special_defense;
    max_speed;

    stat_stages = {
        attack: 0,
        defense: 0,
        speed: 0,
        special_attack: 0,
        special_defense: 0,
        accuracy: 0,
        evasion: 0,
    };

    state;

    battle_moves;

    cant_attack_reason: string[] = [];

    constructor(pokemon: IBattlePokemonReady) {
        super(pokemon);

        Object.keys(pokemon).forEach(prop => this[prop] = pokemon[prop]);

        this.battle_moves = (pokemon.moves as string[]).map(move => MoveLoad(move));

    }

    getBattlePokemon(): IBattlePokemonReady {
        const battleReady = this.getBattlePokemonReady();

        console.log("stat stages", this.name, this.stat_stages);

        Object.keys(battleReady).forEach(prop => {
            battleReady[prop] = this[prop];
        });

        return battleReady;
    }

    getHPPercentage(): number {
        return Math.floor((this.hp / this.max_hp) * 100)
    }

    takeDamage(amt): string[] {

        if (amt <= 0) {
            return [ ];
        }

        const res = [ this.name + " took " + Numeral(amt).format('0,0') + " damage!" + `` ];

        // subtract damage from hp stat
        const hp_left = Math.max(0, Math.round(this.hp - amt));

        this.hp = hp_left;

        const will_to_fight = this.getHPPercentage();

        // when hp reaches 0, the Pokemon faints
        if (will_to_fight <= 0) {
            res.push(this.name + " has fainted!" + ``);
        } else {
            res.push(this.name + " has " + will_to_fight + "% hp left" + ``);
        }

        return res;
    }

    // Boost a single stat
    boostStat(stat, amt): string[] {
        let stat2, stat3;
        const res: string[] = [ ];

        const stat_boost_amt = Stats.stat_boost_stages[amt];

        const definitions = {
            "raised": {
                stat_boost_limited: () => (stat2 >= Math.floor(stat3 * 4)),
                limit_text: "won't go any higher!",
                hp_effect_text: "gained",
                stage_effect_text: "raised"
            },
            "lowered": {
                stat_boost_limited: () => (stat2 <= Math.floor(stat3 * 0.28)),
                limit_text: "won't go any lower!",
                hp_effect_text: "lost",
                stage_effect_text: "lowered"
            }
        }[(amt >= 0) ? 'raised' : 'lowerd']

        stat2 = this[stat];
        stat3 = this['max_' + stat];

        if (definitions.stat_boost_limited()) { // If stat can no longer go higher or lower
            res.push(this.name + "'s " + stat + " was unaffected"); //don't allow the stat to be changed
        } else {
            if (stat === "hp") { //if hp is being raised (healed)...
                let healed_amt = amt;

                if (this.hp + amt >= this.max_hp) { //if heal amount is greater than the max_hp
                    healed_amt = this.max_hp - this.hp
                    this.hp = this.max_hp; //cap the hp at its max value to prevent over-healing
                } else {
                    this.hp += healed_amt; //otherwise, heal the given amount   
                }

                res.push(`${this.name} ${definitions.hp_effect_text} ${healed_amt} hp`);
            } else { //for all other stats, raise the stat by the given amount

                //(((2 * level + 10)/250) * (stat to be changed) * (amount to change stat by) + 2);
                // let raised = Math.floor((((2 * this.level + 10) / 250) * (stat2) * (stat_boost_amt) + 2));
                this.stat_stages[stat] += amt;

                console.log('stat_stages', this.stat_stages);

                const stage = this.stat_stages[stat];

                let new_stat = stat3 * Stats.stat_boost_stages[stage]

                this[stat] = new_stat;

                res.push(this.name + "'s " + stat + ' stage is now ' + definitions.stage_effect_text + " by " + Math.abs(stage));
            }
        }

        return res;
    }

    canAttack() {
        let res: string = "";
        let can_attack = true;

        // Check attack stopping conditions
        if(this.status_condition) {
            this.status_condition.length--;

            switch (this.status_condition.type) {
                case 'par':
                    can_attack = RandomOutcome({ 0: 0.5, 1: 0.5 }) === 1;
                    if (!can_attack) {
                        res = "paralyzed and couldnt move";
                    }
                    break;
                case 'frz':
                    res = "frozen solid";
                    can_attack = false;
                    break;
                case 'slp':
                    res = "asleep";
                    can_attack = false;
                    break;
            }

            if (this.status_condition.length <= 0) {
                this.status_condition = null;
            }
        }

        if (!can_attack) {
            this.cant_attack_reason.push(`${this.name} is ${res}`)
        }

        return can_attack;
    }

    // call on a Pokemon to initiate an attack:
    Attack(move: Move, target: BattlePokemon) {
        const self = this;
        const intended_target: BattlePokemon = new BattlePokemon(target.getBattlePokemon());
        let used_on_self = false;

        let attack_log: string[] = [ ];

        const respond = (logs) => {
            if (used_on_self) {
                console.log('Inteded target', intended_target.name, 'actual target', target.name);
                target = intended_target;
            }
            return { attacker: this.getBattlePokemon(), defender: target.getBattlePokemon(), logs: attack_log }
        }
        
        if (!this.canAttack()) {
            return respond(this.cant_attack_reason);
        }

        if (self.getHPPercentage() > 0) {

            const move_index = this.moves.map(m => m.toLowerCase().replace(/\s/, '')).indexOf(move.name.toLowerCase().replace(/\s/, ''));

            let move_pp_left = (move.pp - this.moves_pp_used[move_index]);

            if (move_pp_left <= 0) {
                attack_log.push(`${move.name} has no more PP left, using struggle instead.`)
                move = new Move(MoveLoad('struggle'));
            } else {
                this.moves_pp_used[move_index] += 1;
            }

            let target_text = "";

            if (move.target && move.target == "self") {
                target = self;
                used_on_self = true;
            } else {
                target_text = " on " + target.name
            }

            if (this.hp <= 0) {
                attack_log[0] = `${this.name} is unable to battle`
            } else {

                attack_log.push(this.name + " used " + move.name + target_text);
                switch (move.category) { //if it is a physical or special attack...
                    case 'Physical':
                    case 'Special':
                        let attack = this.attack; //attack stat of the pokemon who is currently attacking
                        let special_attack = this.special_attack; //special attack stat of the pokemon who is currently attacking
                        if(move.didHit(target, self)) {
                            //call Move.dealdamage method
                            attack_log = attack_log.concat(move.dealDamage(target, attack, special_attack, self)); 
                        } else {
                            attack_log.push(`${self.name} missed!`);
                        }
                        break;
                }

                // If move has healing property
                if (move.heal) {

                    // Add it as a move boost onto the target
                    move.boosts = {
                        hp: (target.max_hp * move.heal)
                    }
                }
            }

            attack_log = attack_log.concat(move.afterHit(target, self));

        } else {
            attack_log.push(`${self.name} is unable to battle...`);
        }

        attack_log = attack_log.concat(this.checkStatusConditions());

        return respond(attack_log)
    };

    checkStatusConditions() {
        let res =  [ ] as string[];

        if (!this.status_condition) return res;
        
        if (this.status_condition.length <= 0) {
            this.status_condition = null;
            return res;
        }

        this.status_condition.length--;

        switch(this.status_condition.type) {
            case 'psn':
            case 'tox':
                res.push(`${this.name} was hurt by poison!`);
                res = res.concat(this.takeDamage(this.max_hp * (1 / 8)));
                break;
        }

        return res;
    }

    //call on a Pokemon to restore its hp with an item:
    heal(item, target) {
        this.inventory -= 1; //subtract from inventory
        item.heal(target); //call the item's heal function
    };
}
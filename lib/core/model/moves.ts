import { IMove } from '../interface/moves';
import { TypeDamageMatrix } from './type-damage';
import { BattlePokemon } from '../model/battle';
import randomOutcome from "random-outcome"

//move constructor function (for damage-dealing moves)
export default class Move implements IMove {
    num;
    accuracy;
    basePower;
    category;
    desc;
    shortDesc;
    id;
    name;
    pp;
    priority;
    flags;
    isZ;
    critRatio;
    target;
    type;
    contestType;
    secondary;
    boosts;
    zMoveBoost;
    heal;
    drain;
    selfdestruct;
    selfHpCost;
    status;

    damage_delt;

    constructor(move: IMove) {
        Object.keys(move).forEach((key) => this[key] = move[key])
    }

    changeStat(stat, amt, direction, target) {

        let boost = {
            "down": () => ( target.lowerStat(stat, amt) ),
            "up": () => ( target.raiseStat(stat, amt) )
        }
        
        return boost[direction]();
    };

    changeStatus(status) { };

    didHit(target: BattlePokemon, user: BattlePokemon): boolean {
        const move_acc_difference = Math.abs(target.accuracy - this.accuracy);

        const acc = user.accuracy - move_acc_difference;
        const eva = user.evasion;

        let hit_chance = acc / eva;
        let miss_chance = 1 - hit_chance;

        let hit_chances = {
            1: hit_chance,
            0: miss_chance
        }

        const hit = randomOutcome(hit_chances) == 1

        return hit;
    }

    dealDamage(target: BattlePokemon, attack: number, special_attack: number, user: BattlePokemon): string[] { //called by Pokemon.attack() method
        let res = [ ] as string[];

        let typeVar; //holds the value for the type part of the calculation
        let stabVar; //holds the value for the type part of the calculation
        let damage;
        //type effectiveness calculation
        //value can be 2 (super effective), 1 (normal effectiveness), or 0.5 (resisted)
        //set up super effective moves
        typeVar = 1;
        stabVar = 1;

        user.types.forEach(t => {
            console.log('t.toLowerCase() == this.type.toLowerCase()', t.toLowerCase() == this.type.toLowerCase(), t.toLowerCase(), this.type.toLowerCase())
            stabVar *= (t.toLowerCase() == this.type.toLowerCase()) ? 2 : 1
        })
        
        target.types.forEach(t => {
            typeVar *= TypeDamageMatrix[this.type.toLowerCase()][t.toLowerCase()];
        })

        if(typeVar >= 2) {
            res.push(`It's super effective!`)
        } else if(typeVar < 1 && typeVar > 0) {
            res.push(`It's not very effective.`)
        } else if (typeVar <= 0) {
            res.push(`It had no effect...`)
        }

        //oficial Pokemon damage calc
        //(((2 * level + 10)/250) * (user attack/target defense) * (moves's base pwr) + 2) * modifiers
        //until I program levels into this script, I will use 25 as the default level for all pokemon
        //modifiers is calculated as STAB * type * crit * other * random #
        //modifiers include critical hits, same-type attack bonus, type effectiveness, and random Numbers
        //the random Number should be between .85 and 1
        let roll = (Math.floor(Math.random() * (100 - 85 + 1)) + 85) / 100; //random Number
        if (this.category === "Physical") {
            damage = Math.floor((((2 * user.level + 10) / 250) * (attack / target.defense) * (this.basePower) + 2) * (roll * (typeVar * stabVar)));
        } else if (this.category === "Special") {
            damage = Math.floor((((2 * user.level + 10) / 250) * (special_attack / target.special_defense) * (this.basePower) + 2) * (roll * (typeVar * stabVar)));
        }
        
        this.damage_delt = damage;

        res = res.concat(target.takeDamage(damage));

        return res;
    };

    boostTarget(target: BattlePokemon): string[] {
        const stats_to_boost = Object.keys(this.boosts);
        let res: string[] = [];

        stats_to_boost.forEach(stat => {
            res = res.concat(target.boostStat(stat, this.boosts[stat]))
        })

        return res;
    }

    afterHit(target: BattlePokemon, user: BattlePokemon): string[] {
        let res: string[] = [ ];

        if(this.selfHpCost) {
            res = res.concat(user.takeDamage(Math.round(user.max_hp * this.selfHpCost)))
        }

        if (this.selfdestruct == 'always') {
            res = res.concat(target.takeDamage(target.max_hp))
        }

        if (this.drain > 0) {
            res = res.concat(user.boostStat('hp', Math.max(1, Math.floor(this.damage_delt * this.drain))))
        }

        if (this.boosts) {
            if(this.target && this.target == 'self') {
                res = res.concat(this.boostTarget(user));
            } else {
                res = res.concat(this.boostTarget(target));
            }
        }

        if(this.status) {
            if(!target.status_condition) {
                target.status_condition = {
                    type: this.status,
                    length: 3
                }

                res.push(`${target.name} is now ${this.status}ed!`);
            } else {
                res.push(`${this.name}'s ${this.status} failed against ${target.name} who is already ${target.status_condition.type}ed...`);
            }
        }

        return res;
    }
}
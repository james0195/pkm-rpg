import Pokemon from "./pokemon";
import Item from "./items";
import Move from "./moves";

import { BattlePokemon } from './battle';

import { TypeDamageMatrix } from "./type-damage";


export {
    Pokemon,
    BattlePokemon,
    Item,
    Move,
    TypeDamageMatrix
};

export default {
    Pokemon,
    BattlePokemon,
    Item,
    Move,
    TypeDamageMatrix
};


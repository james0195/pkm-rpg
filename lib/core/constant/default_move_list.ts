export const KantoDefaultMoveList = {
  bulbasaur: [
    'vine whip',
    'synthesis',
    'tackle',
    'petal dance'
  ],
  ivysaur: [
    'vine whip',
    'synthesis',
    'tackle',
    'petal dance'
  ],
  venusaur: [
    'vine whip',
    'synthesis',
    'tackle',
    'petal dance'
  ],
  charmander: [
    'fire punch',
    'ember',
    'fire spin',
    'scratch'
  ],
  charmeleon: [
    'fire punch',
    'ember',
    'fire spin',
    'scratch'
  ],
  charizard: [
    'fire punch',
    'ember',
    'fire spin',
    'scratch'
  ],
  squirtle: [
    'bubble',
    'whirlpool',
    'withdraw',
    'tackle'
  ],
  wartortle: [
    'bubble',
    'whirlpool',
    'withdraw',
    'tackle'
  ],
  blastoise: [
    'bubble',
    'whirlpool',
    'withdraw',
    'tackle'
  ],
  caterpie: [
    'string shot',
    'take down',
    'lunge',
    'tackle'
  ],
  metapod: [
    'string shot',
    'take down',
    'lunge',
    'tackle'
  ],
  butterfree: [
    'string shot',
    'take down',
    'lunge',
    'tackle'
  ],
  weedle: [
    'string shot',
    'tackle',
    'lunge',
    'poison sting'
  ],
  kakuna: [
    'string shot',
    'tackle',
    'lunge',
    'poison sting'
  ],
  beedrill: [
    'string shot',
    'tackle',
    'lunge',
    'poison sting'
  ],
  pidgey: [
    'gust',
    'whirlwind',
    'tailwind',
    'twister'
  ],
  pidgeotto: [
    'gust',
    'whirlwind',
    'tailwind',
    'twister'
  ],
  pidgeot: [
    'gust',
    'whirlwind',
    'tailwind',
    'twister'
  ],
  rattata: [
    'tackle',
    'take down',
    'crunch',
    'scratch'
  ],
  raticate: [
    'tackle',
    'take down',
    'crunch',
    'scratch'
  ],
  spearow: [
    'take down',
    'aerial ace',
    'growl',
    'fly'
  ],
  fearow: [
    'take down',
    'aerial ace',
    'growl',
    'fly'
  ],
  ekans: [
    'poison sting',
    'mud bomb',
    'rock tomb',
    'mud-slap'
  ],
  arbok: [
    'poison sting',
    'mud bomb',
    'rock tomb',
    'mud-slap'
  ],
  pikachu: [
    'iron tail',
    'thunder shock',
    'thunderbolt',
    'spark'
  ],
  raichu: [
    'iron tail',
    'thunder shock',
    'thunderbolt',
    'spark'
  ],
  sandshrew: [
    'scratch',
    'metal claw',
    'dig',
    'sandstorm'
  ],
  sandslash: [
    'scratch',
    'metal claw',
    'dig',
    'sandstorm'
  ],
  'nidoran♀': [
    'mud-slap',
    'growl',
    'flatter',
    'focus energy'
  ],
  nidorina: [
    'mud-slap',
    'growl',
    'flatter',
    'focus energy'
  ],
  nidoqueen: [
    'mud-slap',
    'growl',
    'flatter',
    'focus energy'
  ],
  'nidoran♂': [
    'take down',
    'toxic',
    'rock smash',
    'iron tail'
  ],
  nidorino: [
    'take down',
    'toxic',
    'rock smash',
    'iron tail'
  ],
  nidoking: [
    'take down',
    'toxic',
    'rock smash',
    'iron tail'
  ],
  clefairy: [
    'follow me',
    'flash',
    'rollout',
    'take down'
  ],
  clefable: [
    'follow me',
    'flash',
    'rollout',
    'take down'
  ],
  vulpix: [
    'flamethrower',
    'ember',
    'charm',
    'roar'
  ],
  ninetales: [
    'flamethrower',
    'ember',
    'charm',
    'roar'
  ],
  jigglypuff: [
    'take down',
    'sing',
    'mega punch',
    'flash'
  ],
  wigglytuff: [
    'take down',
    'sing',
    'mega punch',
    'flash'
  ],
  zubat: [
    'taunt',
    'whirlwind',
    'gust',
    'supersonic'
  ],
  golbat: [
    'taunt',
    'whirlwind',
    'gust',
    'supersonic'
  ],
  oddish: [
    'poison powder',
    'stun spore',
    'flash',
    'petal dance'
  ],
  gloom: [
    'poison powder',
    'stun spore',
    'flash',
    'petal dance'
  ],
  vileplume: [
    'poison powder',
    'stun spore',
    'flash',
    'petal dance'
  ],
  paras: [
    'leech seed',
    'poison powder',
    'light screen',
    'stun spore'
  ],
  parasect: [
    'leech seed',
    'poison powder',
    'light screen',
    'stun spore'
  ],
  venonat: [
    'string shot',
    'poison powder',
    'flash',
    'stun spore'
  ],
  venomoth: [
    'string shot',
    'poison powder',
    'flash',
    'stun spore'
  ],
  diglett: [
    'dig',
    'mud bomb',
    'stealth rock',
    'growl'
  ],
  dugtrio: [
    'dig',
    'mud bomb',
    'stealth rock',
    'growl'
  ],
  meowth: [
    'scratch',
    'taunt',
    'fury swipes',
    'u-turn'
  ],
  persian: [
    'scratch',
    'taunt',
    'fury swipes',
    'u-turn'
  ],
  psyduck: [
    'confuse ray',
    'light screen',
    'icy wind',
    'whirlpool'
  ],
  golduck: [
    'confuse ray',
    'light screen',
    'icy wind',
    'whirlpool'
  ],
  mankey: [
    'taunt',
    'submission',
    'scratch',
    'leer'
  ],
  primeape: [
    'taunt',
    'submission',
    'scratch',
    'leer'
  ],
  growlithe: [
    'flamethrower',
    'ember',
    'flame charge',
    'roar'
  ],
  arcanine: [
    'flamethrower',
    'ember',
    'flame charge',
    'roar'
  ],
  poliwag: [
    'bubble',
    'waterfall',
    'flail',
    'surf'
  ],
  poliwhirl: [
    'bubble',
    'waterfall',
    'flail',
    'surf'
  ],
  poliwrath: [
    'bubble',
    'waterfall',
    'flail',
    'surf'
  ],
  abra: [
    'teleport',
    'psybeam',
    'flash',
    'light screen'
  ],
  kadabra: [
    'teleport',
    'psybeam',
    'flash',
    'light screen'
  ],
  alakazam: [
    'teleport',
    'psybeam',
    'flash',
    'light screen'
  ],
  machop: [
    'rolling kick',
    'submission',
    'bulk up',
    'rock smash'
  ],
  machoke: [
    'rolling kick',
    'submission',
    'bulk up',
    'rock smash'
  ],
  machamp: [
    'rolling kick',
    'submission',
    'bulk up',
    'rock smash'
  ],
  bellsprout: [
    'razor leaf',
    'synthesis',
    'sludge bomb',
    'bullet seed'
  ],
  weepinbell: [
    'razor leaf',
    'synthesis',
    'sludge bomb',
    'bullet seed'
  ],
  victreebel: [
    'razor leaf',
    'synthesis',
    'sludge bomb',
    'bullet seed'
  ],
  tentacool: [
    'bubble',
    'barrier',
    'sludge bomb',
    'whirlpool'
  ],
  tentacruel: [
    'bubble',
    'barrier',
    'sludge bomb',
    'whirlpool'
  ],
  geodude: [
    'flail',
    'dig',
    'tackle',
    'harden'
  ],
  graveler: [
    'flail',
    'dig',
    'tackle',
    'harden'
  ],
  golem: [
    'flail',
    'dig',
    'tackle',
    'harden'
  ],
  ponyta: [
    'flame charge',
    'bounce',
    'fire spin',
    'stomp'
  ],
  rapidash: [
    'flame charge',
    'bounce',
    'fire spin',
    'stomp'
  ],
  slowpoke: [
    'tackle',
    'zen headbutt',
    'light screen',
    'ice punch'
  ],
  slowbro: [
    'tackle',
    'zen headbutt',
    'light screen',
    'ice punch'
  ],
  magnemite: [
    'flash cannon',
    'metal sound',
    'supersonic',
    'electroweb'
  ],
  magneton: [
    'flash cannon',
    'metal sound',
    'supersonic',
    'electroweb'
  ],
  'farfetch\'d': [
    'sky attack',
    'sucker punch',
    'u-turn',
    'gust'
  ],
  doduo: [
    'take down',
    'agility',
    'drill peck',
    'growl'
  ],
  dodrio: [
    'take down',
    'agility',
    'drill peck',
    'growl'
  ],
  seel: [
    'take down',
    'blizzard',
    'surf',
    'lick'
  ],
  dewgong: [
    'take down',
    'blizzard',
    'surf',
    'lick'
  ],
  grimer: [
    'tackle',
    'toxic',
    'slam',
    'acid armor'
  ],
  muk: [
    'tackle',
    'toxic',
    'slam',
    'acid armor'
  ],
  shellder: [
    'withdraw',
    'bubble',
    'ice beam',
    'barrier'
  ],
  cloyster: [
    'withdraw',
    'bubble',
    'ice beam',
    'barrier'
  ],
  gastly: [
    'lick',
    'astonish',
    'smog',
    'sucker punch'
  ],
  haunter: [
    'lick',
    'astonish',
    'smog',
    'sucker punch'
  ],
  gengar: [
    'lick',
    'astonish',
    'smog',
    'sucker punch'
  ],
  onix: [
    'flash cannon',
    'stealth rock',
    'sandstorm',
    'harden'
  ],
  drowzee: [
    'psybeam',
    'taunt',
    'light screen',
    'barrier'
  ],
  hypno: [
    'psybeam',
    'taunt',
    'light screen',
    'barrier'
  ],
  krabby: [
    'surf',
    'flail',
    'stomp',
    'harden'
  ],
  kingler: [
    'surf',
    'flail',
    'stomp',
    'harden'
  ],
  voltorb: [
    'self destruct',
    'taunt',
    'thunder shock',
    'rollout'
  ],
  electrode: [
    'self destruct',
    'taunt',
    'thunder shock',
    'rollout'
  ],
  exeggcute: [
    'psychic',
    'sludge bomb',
    'leech seed',
    'flash'
  ],
  exeggutor: [
    'psychic',
    'sludge bomb',
    'leech seed',
    'flash'
  ],
  cubone: [
    'bonemerang',
    'growl',
    'sandstorm',
    'focus energy'
  ],
  marowak: [
    'bonemerang',
    'growl',
    'sandstorm',
    'focus energy'
  ],
  hitmonlee: [
    'rolling kick',
    'submission',
    'close combat',
    'focus energy'
  ],
  hitmonchan: [
    'dynamic punch',
    'comet punch',
    'mega punch',
    'fire punch'
  ],
  lickitung: [
    'lick',
    'slam',
    'stomp',
    'mega punch'
  ],
  koffing: [
    'flamethrower',
    'smog',
    'sludge bomb',
    'self destruct'
  ],
  weezing: [
    'flamethrower',
    'smog',
    'sludge bomb',
    'self destruct'
  ],
  rhyhorn: [
    'roar',
    'take down',
    'stomp',
    'crunch'
  ],
  rhydon: [
    'roar',
    'take down',
    'stomp',
    'crunch'
  ],
  chansey: [
    'take down',
    'mega punch',
    'light screen',
    'egg bomb'
  ],
  tangela: [
    'vine whip',
    'slam',
    'leech seed',
    'synthesis'
  ],
  kangaskhan: [
    'take down',
    'outrage',
    'stomp',
    'mega punch'
  ],
  horsea: [
    'bubble',
    'icy wind',
    'agility',
    'whirlpool'
  ],
  seadra: [
    'bubble',
    'icy wind',
    'agility',
    'whirlpool'
  ],
  goldeen: [
    'splash',
    'aqua jet',
    'surf',
    'take down'
  ],
  seaking: [
    'splash',
    'aqua jet',
    'surf',
    'take down'
  ],
  staryu: [
    'icy wind',
    'flash cannon',
    'ice beam',
    'twister'
  ],
  starmie: [
    'icy wind',
    'flash cannon',
    'ice beam',
    'twister'
  ],
  'mr. mime': [
    'substitute',
    'taunt',
    'psychic',
    'barrier'
  ],
  scyther: [
    'lunge',
    'swords dance',
    'steel wing',
    'tailwind'
  ],
  jynx: [
    'psychic',
    'blizzard',
    'ice beam',
    'flash'
  ],
  electabuzz: [
    'thunder',
    'thunder shock',
    'thunderbolt',
    'charge'
  ],
  magmar: [
    'flamethrower',
    'ember',
    'follow me',
    'flare blitz'
  ],
  pinsir: [
    'take down',
    'submission',
    'dig',
    'focus energy'
  ],
  tauros: [
    'hyper beam',
    'outrage',
    'blizzard',
    'earthquake'
  ],
  magikarp: [
    'splash',
    'bounce',
    'tackle',
    'flail'
  ],
  gyarados: [
    'splash',
    'bounce',
    'tackle',
    'flail'
  ],
  lapras: [
    'hydro pump',
    'whirlpool',
    'blizzard',
    'ice beam'
  ],
  ditto: [
    'transform',
    '',
    '',
    ''
  ],
  eevee: [
    'flail',
    'take down',
    'charm',
    'dig'
  ],
  vaporeon: [
    'flail',
    'take down',
    'charm',
    'dig'
  ],
  jolteon: [
    'flail',
    'take down',
    'charm',
    'dig'
  ],
  flareon: [
    'flail',
    'take down',
    'charm',
    'dig'
  ],
  porygon: [
    'thunderbolt',
    'flamethrower',
    'ice beam',
    'hyper beam'
  ],
  omanyte: [
    'bubble',
    'icy wind',
    'whirlpool',
    'spikes'
  ],
  omastar: [
    'bubble',
    'icy wind',
    'whirlpool',
    'spikes'
  ],
  kabuto: [
    'aqua jet',
    'withdraw',
    'waterfall',
    'metal sound'
  ],
  kabutops: [
    'aqua jet',
    'withdraw',
    'waterfall',
    'metal sound'
  ],
  aerodactyl: [
    'crunch',
    'take down',
    'fly',
    'earthquake'
  ],
  snorlax: [
    'take down',
    'mega punch',
    'outrage',
    'earthquake'
  ],
  articuno: [
    'sky attack',
    'ice beam',
    'blizzard',
    'agility'
  ],
  zapdos: [
    'sky attack',
    'thunder',
    'charge',
    'thunderbolt'
  ],
  moltres: [
    'sky attack',
    'flamethrower',
    'heat wave',
    'leer'
  ],
  dratini: [
    'agility',
    'extreme speed',
    'dragon rush',
    'dragon dance'
  ],
  dragonair: [
    'agility',
    'extreme speed',
    'dragon rush',
    'dragon dance'
  ],
  dragonite: [
    'agility',
    'extreme speed',
    'dragon rush',
    'dragon dance'
  ],
  mewtwo: [
    'psychic',
    'shadow ball',
    'thunderbolt',
    'calm mind'
  ],
  mew: [
    'psychic',
    'flash cannon',
    'amnesia',
    'heat wave'
  ]
};

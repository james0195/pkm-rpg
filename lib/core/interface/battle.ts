import { IPokemon } from "./pokemon";
import { IMove } from ".";
import { IStatStages } from "./stats";

export interface IBattlePokemonReady extends IPokemon {
    status_effect?: string;

    accuracy: number;
    evasion: number;
    max_accuracy: number;
    max_evasion: number;

    max_hp: number;
    max_attack: number;
    max_defense: number;
    max_special_attack: number;
    max_special_defense: number;
    max_speed: number;

    moves: string[];
    battle_moves: IMove[];
}

export interface IBattlePokemon extends IBattlePokemonReady {
    stat_stages: IStatStages;
    status_condition?: object;
}
export interface ITypeDamageChart {
    normal: Number;
    fire: Number;
    fighting: Number;
    water: Number;
    flying: Number;
    grass: Number;
    poison: Number;
    electric: Number;
    ground: Number;
    psychic: Number;
    rock: Number;
    ice: Number;
    bug: Number;
    dragon: Number;
    ghost: Number;
    dark: Number;
    steel: Number;
    fairy: Number,
}

export interface ITypeDamageMatrix {
    normal: ITypeDamageChart;
    fire: ITypeDamageChart;
    fighting: ITypeDamageChart;
    water: ITypeDamageChart;
    flying: ITypeDamageChart;
    grass: ITypeDamageChart;
    poison: ITypeDamageChart;
    electric: ITypeDamageChart;
    ground: ITypeDamageChart;
    psychic: ITypeDamageChart;
    rock: ITypeDamageChart;
    ice: ITypeDamageChart;
    bug: ITypeDamageChart;
    dragon: ITypeDamageChart;
    ghost: ITypeDamageChart;
    dark: ITypeDamageChart;
    steel: ITypeDamageChart;
    fairy: ITypeDamageChart,
}
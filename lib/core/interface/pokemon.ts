import { IMove } from "./moves";

export interface IPokemon {
    pokemon_id?: number;
    name?: string;

    types: string[];

    level?: number;
    color?: string;

    hp: number;
    attack: number;
    defense: number;
    special_attack: number;
    special_defense: number;
    speed: number;

    moves: string[] | IMove[];

    moves_pp_used?: number[];

    inventory: number;
    // Set this to true if you don't want default stats set on BattlePokemon pokemon creation
    is_battle_ready?: boolean;
}

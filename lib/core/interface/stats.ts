export interface  IStatStages {
    attack: number;
    defense: number;
    speed: number;
    special_attack: number;
    special_defense: number;
}
import { IBattlePokemonReady } from '../interface/battle';

export interface IPokemonCreateRequest {
    // A name or ID is required although optional here is to let req set either name or id
    // Name of pokemon (will try and find it in pokedex)
    name?: string;
    color?: string;

    // Pokedex number of pokemon (will try and find it in pokedex)
    pokemon_id?: number;

    // Stats to give created pokemon
    level?: number;
    hp?: number;
    attack?: number;
    defense?: number;
    special_attack?: number;
    special_defense?: number;
    speed?: number;
    moves?: string[];
}

export interface IPokemonAttackRequest {
    attacker_move_used: number;
    defender_move_used: number;
    attacker: IBattlePokemonReady;
    defender: IBattlePokemonReady;
}

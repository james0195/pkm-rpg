export interface IItem {
    name: String;
    healAmount: Number; //how much does it heal
    healStat: String; //which stat does it heal
}
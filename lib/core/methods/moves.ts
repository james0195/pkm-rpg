import MoveIndex from '../constant/moves';
import { IMove } from '../interface';
import { Move } from '../model';

export const MoveLoad = (move_name = "struggle"): IMove => {

    const move = MoveIndex[(move_name).replace(/\s+/g, '')];

    return new Move(move);
};


export default { MoveLoad };

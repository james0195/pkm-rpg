import { BattlePokemon } from '../core/model/battle';

import { IBattlePokemonReady } from '../core/interface/battle';

import { IPokemonAttackRequest } from '../core/interface/request';

export function pokemonCreate(input: IBattlePokemonReady): BattlePokemon {
    return new BattlePokemon(input);
}

export function pokemonAttack(input: IPokemonAttackRequest): any {
    const props = ["attacker_move_used", "defender_move_used", "attacker", "defender"];

    let res = { attacker: input.attacker, defender: input.defender, logs: [ ] as string[] };
    try {
        if ([props].map(key => Object.getOwnPropertyNames(input).indexOf(key as any) !== -1).every(val =>  val === true)) {
            throw new Error("Missing required properties");
        }

        const attacker_pokemon = new BattlePokemon(input.attacker);
        const defender_pokemon = new BattlePokemon(input.defender);

        res = attacker_pokemon.Attack(attacker_pokemon.battle_moves[input.attacker_move_used], defender_pokemon);
    } catch (e) {
        console.log("[ERROR]");
        console.error(e);
    }



    return res;
}

export default { pokemonCreate, pokemonAttack };

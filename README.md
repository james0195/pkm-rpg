<h1 align="center"> Pokemon RPG Kit - Gotta use em all </h1>

<p align="center"> A plethora of Pokemon RPG ready modules </p>

<hr/>

<p> For the longest time since I can remmeber I have always used Pokemon as a means to learn how to build apps on the web. </p>
<p> However fun it was; One thing I hated was to have to write Core logic from the ground up! </p>
<p> Since I build apps with the power of NPM I have decided to create this package to make the task easier. </p>
<p> Enjoy! </p>

<h3> Download & Installation </h3>

```shell
$ npm i -s pkm-rpg
```
### Import
```typescript
import * as pkmrpg  from 'pkm-rpg'
```
### Require
```javascript
var pkmrpg = require('pkm-rpg')
```

<h3> List of features </h3>

<ul>
  <li>
    <p>Pokemon Methods</p>
      <ul>
        <li>Pokemon Creation</li>
      </ul>
  </li>

  <li>
      <p>Battle Methods</p>
      <ul>
        <li>Pokemon Creation</li>
        <li>Pokemon Attack</li>
      </ul>
    </li>
</ul>

<h3> Pokemon : Pokemon Creation </h3>

### Interface

```typescript
interface IPokemonCreateRequest {
    // A name or ID is required although optional here is to let req set either name or id
    
    // Name of pokemon (will try and find it in pokedex)
    name?: string,
    // Pokedex number of pokemon (will try and find it in pokedex)
    id?: number,

    // Stats to give created pokemon
    level?: number,
    hp?: number,
    attack?: number,
    defense?: number,
    special_attack?: number,
    special_defense?: number,
    speed?: number,
    moves?: string[]
}
```

### Ussage
```javascript
var Pikachu = pkmrpg.Pokemon.pokemonCreate({
    id: 25,
    level: 100,
    moves: ['thunder bolt', 'iron tail', 'quick attack', 'tackle']
});
```

<h3> Battle : Pokemon Creation </h3>

```typescript
interface IPokemon {
    name: string;
    types: string[];

    level?: number;

    hp: number;
    attack: number;
    defense: number;
    special_attack: number;
    special_defense: number;
    speed: number;

    moves: string[] | IMove[];
    moves_pp_used?: number[];

    inventory: number;
    // Set this to true if you don't want default stats set on BattlePokemon pokemon creation
    is_battle_ready?: boolean;
}

interface IBattlePokemonReady extends IPokemon {
    status_effect?: string;

    accuracy: number;
    evasion: number;
    max_accuracy: number;
    max_evasion: number;

    max_hp: number;
    max_attack: number;
    max_defense: number;
    max_special_attack: number;
    max_special_defense: number;
    max_speed: number;

    moves: string[];
}
```

### Ussage
```javascript
var Pikachu = pkmrpg.Battle.pokemonCreate({
    name: 'Pikachu',
    types: [ 'Electric' ],
    level: 100,
    hp: 700,
    attack: 1100,
    defense: 800,
    special_attack: 1000,
    special_defense: 1000,
    speed: 1800,
    moves: [ 'thunder bolt', 'iron tail', 'quick attack', 'tackle' ],
    inventory: 1,
    status_effect: '',
    accuracy: 100,
    evasion: 100,
    max_accuracy: 100,
    max_evasion: 100,
    max_hp: 700,
    max_attack: 1100,
    max_defense: 800,
    max_special_attack: 1000,
    max_special_defense: 1000,
    max_speed: 1800,
    moves_pp_used: [ 0, 0, 0, 0 ],
    is_battle_ready: true
  });
```
### Chain Ussage
```javascript
var Pikachu = pkmrpg.Pokemon.pokemonCreate({...});
var BattlePikachu = pkmrpg.Battle.pokemonCreate(Pikachu.getBattlePokemonReady());
```

<h3> Ussage - Battle : Pokemon Attack </h3>

```javascript

var Onyx = pkmrpg.Pokemon.pokemonCreate({...});
var BattleOnyx = pkmrpg.Battle.pokemonCreate(Onyx.getBattlePokemonReady());

var Pikachu = pkmrpg.Pokemon.pokemonCreate({...});
var BattlePikachu = pkmrpg.Battle.pokemonCreate(Pikachu.getBattlePokemonReady());

BattlePikachu.Attack(BattlePikachu.battle_moves[2], BattleOnyx) // Returns attack log (text)
```

<h3>Contributing</h3>

Pull requests are the best way to propose changes to the codebase (we use [Github Flow](https://guides.github.com/introduction/flow/index.html)). We actively welcome your pull requests:

1. Fork the repo and create your branch from `master`.
2. If you've added code that should be tested, add tests.
3. If you've changed APIs, update the documentation.
4. Ensure the test suite passes.
5. Make sure your code lints.
6. Issue that pull request!

<h3>Authors or Acknowledgments</h3>
<ul>
  <li>jamezilla0</li>
</ul>

<h3>License</h3>

This project is licensed under the MIT License
